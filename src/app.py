import json

from flask import Flask, render_template, request
from flask_swagger_ui import get_swaggerui_blueprint
import os
from ExcellExtractor import ExcellExtractor
import time


app = Flask(__name__)

@app.route('/', methods=['POST', 'GET'])
def index():

        return render_template('index.html')

SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Python-Flask-REST"
    }
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)

